import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { catchError, map, retry } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { timeout } from 'q';

@Injectable({
  providedIn: 'root'
})
export class MainService {

  private httpHeaders = new HttpHeaders();

  constructor(private http: HttpClient) { }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(`error from operation: ${operation} is ${error.message}`);

      // console.log(`error message is: ${error.message}`);

      return of(result as T);
    };
  }

  get(url: string, operation: string, headers: HttpHeaders) {
    return this.http.get(url, { headers });
  }

  /*  post(url: string, body: any, operation: string, headers?: HttpHeaders) {
      return this.http.post(url, body, { headers }).pipe(
        map((res) => {
          return res;
        }),
        catchError(this.handleError(operation))
      );
    }*/

  downloadFile(url: string, headers: HttpHeaders) {
    return this.http.post(url, null, {
      responseType: 'blob',
      headers
    });
  }

  post(url: string, body: any, operation: string, headers?: HttpHeaders): any {

    return this.http.post(url, body, { headers });
  }

  put(url: string, body: any, headers: HttpHeaders, operation: string) {
    return this.http.put(url, body, { headers });
  }

  del(url: string, headers: HttpHeaders, operation: string) {
    return this.http.delete(url, { headers });
  }

}
