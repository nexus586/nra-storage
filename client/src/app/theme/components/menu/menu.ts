import { Menu } from './menu.model';

export const AdminMenu = [
    new Menu(2, 'Users', '/users', null, 'supervisor_account', null, false, 0),
    new Menu(3, 'Files', '/', null, 'folder', null, false, 0),
];

export const UserMenu = [
    new Menu(2, 'Files', '/', null, 'folder', null, false, 0),
];
