import { Injectable } from '@angular/core';
import { MainService } from './main.service';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private baseUrl = 'http://localhost:5000/api/v1/';
  private headers = new HttpHeaders();

  constructor(private service: MainService) { }

  doLogin(body: any): any {
    const loginUrl = `${this.baseUrl}users/login`;
    // console.log(body);
    // this.headers.append('Content-Type', 'application/json');
    return this.service.post(loginUrl, body, 'login');
  }
}
