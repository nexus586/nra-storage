import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AppSettings } from '../../app.settings';
import { Settings } from '../../app.settings.model';
import { User } from './user.model';
import { UsersService } from './users.service';
import { UserDialogComponent } from './user-dialog/user-dialog.component';
import { MainService } from 'src/app/main.service';
import { HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AppConstants } from '../app.constants';
import { DeleteUserComponent } from './delete-user/delete-user.component';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [UsersService]
})
export class UsersComponent implements OnInit {

    private users = [];
    public searchText: string;
    public page: any;
    public settings: Settings;
    public showSearch = false;
    public viewType = 'grid';
    constructor(
        public appSettings: AppSettings,
        public dialog: MatDialog,
        public service: MainService,
        private router: Router,
        private toastr: ToastrManager) {
        this.settings = this.appSettings.settings;
    }

    ngOnInit() {
        this.getUsers();
    }

    public getUsers(): void {

        const headers = new HttpHeaders({ 'authorization': `Bearer ${sessionStorage.getItem('token')}` });

        this.service.get(AppConstants.FIND_ALL_USERS_URL, 'fetching users', headers).subscribe((res: any) => {
            this.users = res.obj;
        }, (err) => {
            const error = err.error.message;

            if (error === 'Your session has expired. Please login') {
                this.toastr.errorToastr('Your session has expired. Please login', 'Session Expired');
                setTimeout(() => {
                    this.router.navigate(['/', 'login']);
                    sessionStorage.clear();
                }, 1000);
            }
        });
    }

    public changeView(viewType) {
        this.viewType = viewType;
        this.showSearch = false;
    }

    public onPageChanged(event) {
        this.page = event;
        this.getUsers();
        document.getElementById('main').scrollTop = 0;
    }

    public openUserDialog(user) {
        const dialogRef = this.dialog.open(UserDialogComponent, {
            data: user
        });
        dialogRef.afterClosed().subscribe(() => {
            this.getUsers();
        });
        // this.showSearch = false;
    }

    public openDeleteUserDialog(user) {
        const dialogRef = this.dialog.open(DeleteUserComponent, {
            maxWidth: '400px',
            width: '400px',
            data: user
        });
        dialogRef.afterClosed().subscribe(() => {
            this.getUsers();
        });
    }

}
