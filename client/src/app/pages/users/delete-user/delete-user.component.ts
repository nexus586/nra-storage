import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { User } from '../user.model';
import { MainService } from 'src/app/main.service';
import { AppConstants } from '../../app.constants';
import { HttpHeaders } from '@angular/common/http';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.scss']
})
export class DeleteUserComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DeleteUserComponent>,
    @Inject(MAT_DIALOG_DATA) public user: any,
    private service: MainService,
    private toastr: ToastrManager
  ) { }

  ngOnInit() {
  }

  deleteUser() {
    const headers = new HttpHeaders({ 'authorization': `Bearer ${sessionStorage.getItem('token')}` });

    const loadToast = this.toastr.infoToastr('Deleting user', 'Delete');

    this.service.del(`${AppConstants.DELETE_USER_URL}?email=${this.user.username}`, headers, '').subscribe((res: any) => {
      this.toastr.dismissToastr(loadToast);

      this.toastr.successToastr(res.info, 'Success');

      this.closeDialog();

    }, (err) => {
      console.error(err);
      this.toastr.errorToastr('Unable to delete user. Please try again later', 'Error');
      this.closeDialog();
    });
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
