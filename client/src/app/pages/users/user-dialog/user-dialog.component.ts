import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from '../user.model';
import { ToastrManager } from 'ng6-toastr-notifications';
import { MainService } from 'src/app/main.service';
import { HttpHeaders } from '@angular/common/http';
import { AppConstants } from '../../app.constants';

@Component({
  selector: 'app-user-dialog',
  templateUrl: './user-dialog.component.html',
  styleUrls: ['./user-dialog.component.scss']
})
export class UserDialogComponent implements OnInit {
  public form: FormGroup;
  public updateForm: FormGroup;
  public passwordHide: boolean = true;

  constructor(
    public dialogRef: MatDialogRef<UserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public user: User,
    public fb: FormBuilder,
    private toastr: ToastrManager,
    private service: MainService
  ) {
    if (!this.user) {
      this.form = this.fb.group({
        id: null,
        username: [null, Validators.compose([Validators.required, Validators.email])],
        password: [null, Validators.compose([Validators.required, Validators.minLength(6)])],
        name: [null, Validators.compose([Validators.required])],
        role: [null, Validators.compose([Validators.required])]
      });
    } else {
      this.updateForm = this.fb.group({
        id: null,
        username: [null, Validators.compose([Validators.required, Validators.email])],
        name: [null, Validators.compose([Validators.required])],
        role: [null, Validators.compose([Validators.required])]
      });
    }

  }

  ngOnInit() {
    if (this.user) {
      this.updateForm.setValue(this.user);
    } else {
      this.user = new User();
    }
  }

  addUser() {
    const user = {
      username: this.form.controls.username.value,
      name: this.form.controls.name.value,
      password: this.form.controls.password.value,
      role: this.form.controls.role.value
    };

    const url = AppConstants.ADD_USER_URL;

    const addToast = this.toastr.infoToastr('Adding new User');
    const auth = `Bearer ${sessionStorage.getItem('token')}`;
    const header = new HttpHeaders({ 'Authorization': auth });


    this.service.post(url, user, 'addUser', header).subscribe(
      (res: any) => {
        this.toastr.dismissToastr(addToast);

        this.toastr.successToastr(res.message, 'Success');
        console.log(res);
        this.close();
      },
      (err: any) => {
        this.toastr.dismissToastr(addToast);

        this.toastr.errorToastr(err.message, 'Error');
        console.error(err);
      }
    );
  }


  close(): void {
    this.dialogRef.close();
  }

}
