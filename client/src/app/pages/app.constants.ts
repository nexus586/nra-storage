export class AppConstants {

    public static HOST = 'localhost';
    public static PORT = 5000;
    public static BASE_URL = `http://${AppConstants.HOST}:${AppConstants.PORT}/api/v1`;

    // User routes
    public static UPLOAD_FILE_URL = `${AppConstants.BASE_URL}/users/uploadFile`;
    public static LOGIN_URL = `${AppConstants.BASE_URL}/users/login`;
    public static UPDATE_PASSWORD_URL = `${AppConstants.BASE_URL}/users/updatePassword`;
    public static VIEW_ALL_FILES_URL = `${AppConstants.BASE_URL}/users/viewAllFiles`;
    public static DOWNLOAD_FILE_URL = `${AppConstants.BASE_URL}/users/file`;
    public static DELETE_FILE_URL = `${AppConstants.BASE_URL}/users/deleteFile`;

    // Admin routes
    public static ADD_USER_URL = `${AppConstants.BASE_URL}/admin/addUser`;
    public static FIND_ALL_USERS_URL = `${AppConstants.BASE_URL}/admin/findAllUsers`;
    public static DELETE_USER_URL = `${AppConstants.BASE_URL}/admin/deleteUser`;

}
