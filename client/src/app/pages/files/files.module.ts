import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FilesRoutingModule } from './files-routing.module';
import { FilesComponent } from './files/files.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FileUploadComponent } from './file-upload/file-upload.component';

import { FileUploadModule } from 'ng2-file-upload';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { FileSaverModule } from 'ngx-filesaver';
import { DeleteFileComponent } from './delete-file/delete-file.component';

import { NgxDocViewerModule } from 'ngx-doc-viewer';

@NgModule({
  declarations: [FilesComponent, FileUploadComponent, DeleteFileComponent],
  imports: [
    CommonModule,
    FilesRoutingModule,
    SharedModule,
    FileUploadModule,
    NgxDatatableModule,
    FileSaverModule,
    NgxDocViewerModule
  ],
  entryComponents: [
    FileUploadComponent,
    DeleteFileComponent
  ]
})
export class FilesModule { }
