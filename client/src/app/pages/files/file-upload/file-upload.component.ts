import { Component, OnInit, Inject, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {
  FileUploader
} from 'ng2-file-upload/ng2-file-upload';
import { FormGroup } from '@angular/forms';
import { AppConstants } from '../../app.constants';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit {
  form: FormGroup;

  private valid = false;

  public uploader: FileUploader = new FileUploader({
    url: AppConstants.UPLOAD_FILE_URL,
    headers: [
      {
        name: 'authorization',
        value: `Bearer ${sessionStorage.getItem('token')}`
      }
    ]
  });

  fileTypes = [
    'application/pdf',
    'application/octet-stream',
    'image/jpg',
    'image/png',
    'image/jpeg'
  ];

  constructor(public dialogRef: MatDialogRef<FileUploadComponent>) { }

  ngOnInit() {
    this.uploader.onAfterAddingFile = file => {
      file.withCredentials = false;
      console.log(`fileType: ${file.file.type}`);
      switch (file.file.type) {
        case 'application/pdf':
        case 'application/octet-stream':
        case 'image/jpeg':
        case 'image/png':
        case 'image/jpg':
        case 'image/gif':
          this.valid = true;
          break;
        default:
          this.valid = false;
          break;
      }
    };

    this.uploader.onCompleteAll = () => {
      this.closeDialog();
    };

  }

  public closeDialog() {
    this.dialogRef.close();
  }
}
