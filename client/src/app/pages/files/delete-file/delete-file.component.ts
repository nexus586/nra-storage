import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FilesComponent } from '../files/files.component';
import { ToastrManager } from 'ng6-toastr-notifications';
import { HttpHeaders } from '@angular/common/http';
import { MainService } from 'src/app/main.service';
import { AppConstants } from '../../app.constants';

@Component({
  selector: 'app-delete-file',
  templateUrl: './delete-file.component.html',
  styleUrls: ['./delete-file.component.scss']
})
export class DeleteFileComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DeleteFileComponent>,
    @Inject(MAT_DIALOG_DATA) public name: string,
    private toastr: ToastrManager,
    private service: MainService) { }

  ngOnInit() {

  }

  closeDialog() {
    this.dialogRef.close();
  }

  deleteFile() {
    console.log(`delete: ${this.name}`);
    const url = `${AppConstants.DELETE_FILE_URL}?name=${this.name}`;
    const headers = new HttpHeaders({
      authorization: `Bearer ${sessionStorage.getItem('token')}`
    });
    const loadToast = this.toastr.infoToastr(
      'Attempting to delete file',
      'Deleting'
    );

    this.service.del(url, headers, 'deleting file').subscribe(
      (res: any) => {
        console.log(res);
        this.dialogRef.close();
        this.toastr.dismissToastr(loadToast);
        this.toastr.successToastr(res.message, 'Success');
      },
      err => {
        this.dialogRef.close();
        this.toastr.dismissToastr(loadToast);
        this.toastr.errorToastr(
          'Unable to delete file. Please try again later',
          'Error'
        );
        console.log(err);
      }
    );
  }
}
