import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FileUploadComponent } from '../file-upload/file-upload.component';
import { MainService } from '../../../main.service';
import { HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { FileSaverService } from 'ngx-filesaver';
import { DeleteFileComponent } from '../delete-file/delete-file.component';
import { AppConstants } from '../../app.constants';

@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.scss']
})
export class FilesComponent implements OnInit {
  public searchText: string;
  private isLoading = true;
  private files = [];
  private role;

  loadingIndicator = true;
  showDoc = false;
  viewer = 'google';
  docUrl = '';

  columns = [
    { prop: 'Id', name: 'ID' },
    { prop: 'name', name: 'File Name' },
    { prop: 'createdAt', name: 'Upload Date' }
  ];

  constructor(
    public dialog: MatDialog,
    private service: MainService,
    private router: Router,
    private toastr: ToastrManager,
    private fileSaver: FileSaverService
  ) { }

  ngOnInit() {
    this.role = sessionStorage.getItem('role');
    this.loadfiles();
  }

  openFileUploadModal() {
    const dialogRef = this.dialog.open(FileUploadComponent, {
      maxWidth: '800px',
      width: '600px'
    });

    dialogRef.afterClosed().subscribe(() => {
      this.loadfiles();
    });
  }

  openDeleteFileModal(value) {

    const dialogRef = this.dialog.open(DeleteFileComponent, {
      maxWidth: '400px',
      width: '400px',
      data: value
    });

    dialogRef.afterClosed().subscribe((r) => {
      this.loadfiles();
    });
  }

  loadfiles() {
    this.loadingIndicator = true;

    const headers = new HttpHeaders({
      authorization: `Bearer ${sessionStorage.getItem('token')}`
    });

    this.service.get(AppConstants.VIEW_ALL_FILES_URL, 'load all files', headers).subscribe(
      (res: any) => {
        this.files = res.obj;
        this.loadingIndicator = false;
        console.log(`response is: ${res.obj}`);
      },
      err => {
        const error = err.error.message;
        this.loadingIndicator = false;

        if (error === 'Your session has expired. Please login') {
          this.toastr.errorToastr(
            'Your session has expired. Please login',
            'Session Expired'
          );
          setTimeout(() => {
            this.router.navigate(['/', 'login']);
            sessionStorage.clear();
          }, 1000);
        }
      }
    );
  }

  onSelect(event) {
    console.log(event);
  }


  download(name) {
    console.log(`clicked: ${name}`);

    const loadToast = this.toastr.infoToastr('Attempting to download file', 'Downloading');

    const headers = new HttpHeaders({
      authorization: `Bearer ${sessionStorage.getItem('token')}`
    });

    this.service.downloadFile(`${AppConstants.DOWNLOAD_FILE_URL}?name=${name}`, headers).subscribe((res: any) => {
      console.log(res);

      this.fileSaver.save(res, name);
      this.toastr.dismissToastr(loadToast);
      this.toastr.successToastr('File retrieved successfully. Your browser will begin download soon.', 'Success');
    }, (err: any) => {
      this.toastr.dismissToastr(loadToast);
      this.toastr.errorToastr('Unable to download file. Please try again later', 'Error');
      console.log(err.error);
    });
  }

  preview(name) {
    const headers = new HttpHeaders({
      authorization: `Bearer ${sessionStorage.getItem('token')}`
    });

    console.log(name);
    const url = `${AppConstants.BASE_URL}/users/previewFile`;

    this.service.get(`${url}?name=${name}`, '', headers).subscribe((res: any) => {
      console.log(res);
      this.docUrl = `file:///${res.url}`;
      this.showDoc = true;
    }, err => {
      console.log(err);
    });
  }
}
