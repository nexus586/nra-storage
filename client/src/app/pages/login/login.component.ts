import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';
import { AppSettings } from '../../app.settings';
import { Settings } from '../../app.settings.model';
import { AppService } from '../../app.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { MainService } from '../..//main.service';
import { AppConstants } from '../app.constants';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  private form: FormGroup;
  private settings: Settings;
  private email = new FormControl('', [Validators.required, Validators.email]);
  private password = new FormControl('', [
    Validators.required,
    Validators.minLength(6)
  ]);

  private clicked = false;

  ngOnInit() {
    this.settings = this.appSettings.settings;
    this.form = this.fb.group({
      email: this.email,
      password: this.password
    });
  }

  constructor(
    private appSettings: AppSettings,
    private fb: FormBuilder,
    private router: Router,
    private service: AppService,
    public toast: ToastrManager,
    private mService: MainService,
  ) {
  }

  public onSubmit(): void {
    const email = this.form.controls.email.value;
    const password = this.form.controls.password.value;

    this.clicked = true;
    const user = {
      email,
      password
    };


    const loginToast = this.toast.infoToastr('Please wait while we log you in', 'Login', { toastTimeout: 1000 });

    this.mService.post(AppConstants.LOGIN_URL, user, 'login').subscribe(
      (res) => {
        const { message, token, isFirstLogin, role } = res;

        console.log(`response is: ${res}`);
        console.log(`role is: ${role}`);

        sessionStorage.setItem('token', token);
        sessionStorage.setItem('role', role);
        sessionStorage.setItem('email', email);

        this.toast.dismissToastr(loginToast);
        this.toast.successToastr(message, 'success');

        if (isFirstLogin) {
          setTimeout(() => {
            this.router.navigate(['/', 'change-password'], { queryParams: email });
          }, 2000);
        } else {
          setTimeout(() => {
            this.router.navigate(['/']);
          }, 1000);
        }

      },
      (err) => {
        this.toast.dismissToastr(loginToast);
        this.clicked = false;
        this.toast.errorToastr(err.error.error, err.error.message);
        console.error(err.error.error);
        console.log(`error is: ${err}`);
      }
    );

  }

  ngAfterViewInit() {
    this.settings.loadingSpinner = false;
  }
}
