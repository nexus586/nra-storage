import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder
} from '@angular/forms';
import { Settings } from 'src/app/app.settings.model';
import { AppSettings } from 'src/app/app.settings';
import { ToastrManager } from 'ng6-toastr-notifications';
import { MainService } from 'src/app/main.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  public form: FormGroup;
  public settings: Settings;
  private email = new FormControl('', [Validators.required, Validators.email]);
  private oldPassword = new FormControl('', [
    Validators.required,
    Validators.minLength(6)
  ]);
  private newPassword = new FormControl('', [
    Validators.required,
    Validators.minLength(6)
  ]);

  constructor(
    private fb: FormBuilder,
    private appSettings: AppSettings,
    private mService: MainService,
    public toast: ToastrManager,
    private router: Router
  ) { }

  ngOnInit() {
    this.settings = this.appSettings.settings;
    this.form = this.fb.group({
      email: this.email,
      oldPassword: this.oldPassword,
      newPassword: this.newPassword
    });
  }

  onSubmit() {

    const user = {
      email: this.form.controls.email.value,
      oldPassword: this.form.controls.oldPassword.value,
      newPassword: this.form.controls.newPassword.value
    };

    const loadingToast = this.toast.infoToastr('updating password', 'updating');

    const url = `http://localhost:5000/api/v1/users/updatePassword`;
    this.mService.post(url, user, 'update-password').subscribe((res) => {
      console.log(res);

      this.toast.dismissToastr(loadingToast);
      this.toast.successToastr('password changed successfully', 'success');

      setTimeout(() => {
        this.toast.dismissToastr(loadingToast);
        this.router.navigate(['/']);
      }, 2000);

    }, err => {
      console.log(err);
    });
  }

  ngAfterViewInit() {
    this.settings.loadingSpinner = false;
  }
}
