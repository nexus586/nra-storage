import { Component, ViewChild } from '@angular/core';
import { AppSettings } from './app.settings';
import { Settings } from './app.settings.model';
import { BnNgIdleService } from 'bn-ng-idle';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public settings: Settings;
  constructor(public appSettings: AppSettings, private idle: BnNgIdleService, private router: Router) {
    this.settings = this.appSettings.settings;
    this.idle.startWatching(300).subscribe(
      (res) => {
        if (res) {
          sessionStorage.clear();
          this.router.navigate(['/', 'login']);
        }
      }
    );
  }

  ngOnInit() { }
}