import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { PagesComponent } from './pages/pages.component';
import { BlankComponent } from './pages/blank/blank.component';
import { NotFoundComponent } from './pages/errors/not-found/not-found.component';
import { ErrorComponent } from './pages/errors/error/error.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { AuthGuard } from './auth.guard';

export const routes: Routes = [
    {
        path: '',
        component: PagesComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: 'users',
                loadChildren: () =>
                    import('./pages/users/users.module').then(m => m.UsersModule),
                data: { breadcrumb: 'Users' }
            },
            {
                path: 'profile',
                loadChildren: () =>
                    import('./pages/profile/profile.module').then(m => m.ProfileModule),
                data: { breadcrumb: 'Profile' }
            },
            {
                path: 'blank',
                component: BlankComponent,
                data: { breadcrumb: 'Blank page' }
            },
            {
                path: '',
                loadChildren: () =>
                    import('./pages/files/files.module').then(m => m.FilesModule),
                data: { breadcrumb: 'Files' }
            },
            {
                path: 'files',
                loadChildren: () =>
                    import('./pages/files/files.module').then(m => m.FilesModule),
                data: { breadcrumb: 'Files' }
            }
        ]
    },
    {
        path: 'login',
        loadChildren: () =>
            import('./pages/login/login.module').then(m => m.LoginModule)
    },
    { path: 'change-password', component: ChangePasswordComponent },
    { path: 'error', component: ErrorComponent, data: { breadcrumb: 'Error' } },
    { path: '**', component: NotFoundComponent },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, {
    // preloadingStrategy: PreloadAllModules,  // <- uncomment this line for disable lazy load
    // useHash: true
    enableTracing: true
});
