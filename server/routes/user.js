const express = require('express');
const router = express.Router();
const Users = require('../models/users');
const httpStatus = require('http-status');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('../config/config');
const _ = require('lodash');
const UploadFile = require('../models/uploadFile');
const multer = require('multer');
const path = require('path');
const fs = require('fs');

// Directory for storing all the files that will be uploaded
const fileDirectory = path.join(__dirname + '../../..' + '/uploads');

// Store creates the directory for the uploads if it does not exist
// It also stores the file with the provided name in the specified folder
const store = multer.diskStorage({
  filename: function(req, file, cb) {
    cb(null, `${file.originalname}`);
  },
  destination: function(req, filename, cb) {
    if (!fs.existsSync(fileDirectory)) {
      fs.mkdirSync(fileDirectory);
    }

    cb(null, fileDirectory);
  }
});

// Tells multer to accept single files for storage
const upload = multer({ storage: store }).single('file');

/***********************************************************************
 *                              * ROUTES
 ***********************************************************************/

// LOGIN ROUTE
router.post('/login', (req, res) => {
  const { email, password } = _.pick(req.body, ['email', 'password']);

  const errors = [];

  if (email === null || email === '') {
    errors.push({ error: 'Email address should not be empty' });
  }

  if (password === null || password === '') {
    errors.push({ error: 'Password should not be empty' });
  }

  if (errors.length > 0) {
    return res.status(httpStatus.NOT_FOUND).send(errors);
  }

  Users.findOne({ where: { username: email } }).then(
    doc => {
      if (doc !== null) {
        bcrypt.compare(password, doc.hash).then(response => {
          if (response === true) {
            const obj = { role: doc.role, username: doc.username, id: doc.id };

            return res.status(httpStatus.OK).json({
              token: jwt.sign({ data: obj }, config.secretToken, {
                expiresIn: 60 * 30
              }),
              message: 'Login successful',
              isFirstLogin: doc.firstLoginAttempt,
              role: doc.role
            });
          } else {
            return res.status(httpStatus.BAD_REQUEST).json({
              message: 'error',
              error: 'invalid password or username'
            });
          }
        });
      } else {
        return res.status(httpStatus.BAD_REQUEST).json({
          message: 'error',
          error: 'invalid password or username'
        });
      }
    },
    error => {
      console.error(error);
      return res
        .status(httpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: 'Unable to login. Please try again later.' });
    }
  );
});

// UPDATE PASSWORD ROUTE
router.post('/updatePassword', (req, res) => {
  const { email, oldPassword, newPassword } = _.pick(req.body, [
    'email',
    'oldPassword',
    'newPassword'
  ]);

  const salt = bcrypt.genSaltSync();
  const hash = bcrypt.hashSync(newPassword, salt);

  const user = {
    hash: hash,
    salt: salt,
    firstLoginAttempt: false
  };

  Users.findOne({ where: { username: email } }).then(
    doc => {
      console.log(`doc element is: ${doc}`);
      if (doc !== null) {
        bcrypt.compare(oldPassword, doc.hash).then(response => {
          console.log(`response from bcrypt is: ${response}`);
          if (response === true) {
            Users.update(user, { where: { username: email } }).then(
              doc => {
                if (doc !== null) {
                  return res.status(httpStatus.OK).json({
                    message: 'success',
                    info: 'Password updated successfully'
                  });
                } else {
                  return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
                    message: 'error',
                    info: 'an unexpected error occurred'
                  });
                }
              },
              err => {
                console.log(err);
                return res.status(httpStatus.OK).json({
                  error: err,
                  message: ''
                });
              }
            );
          } else {
            return res.status(httpStatus.BAD_REQUEST).json({
              error: 'Invalid password or username',
              message: 'Error'
            });
          }
        });
      } else {
        return res
          .status(httpStatus.NOT_FOUND)
          .json({ message: 'error', info: 'User not found' });
      }
    },
    err => {
      console.log(`err is: ${err}`);
      return res
        .status(httpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: 'error', info: 'An unexpected error occurred' });
    }
  );

  Users.update(user, { where: { username: email } }).then(
    doc => {
      console.log(doc);
    },
    err => {
      console.log(err);
    }
  );
});

// UPLOAD FILE ROUTE
router.post('/uploadFile', verifyUser, (req, res) => {
  upload(req, res, err => {
    if (err) {
      console.log(`err: ${err}`);
      return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
        message:
          'An unexpected error occurred while uploading your file. Please try again later.'
      });
    } else {
      console.log(req.file);

      const file = {
        name: req.file.originalname,
        size: req.file.size,
        uploadedBy: res.user_id,
        mimeType: req.file.mimetype,
        encoding: req.file.encoding
      };

      UploadFile.create(file).then(
        response => {
          console.log(`response is: ${response}`);
          res.send('success');
        },
        err => {
          console.log(`error is: ${err}`);
          res.send(err);
        }
      );
    }
  });
});

// VIEW ALL FILES ROUTE
router.get('/viewAllFiles', verifyUser, (req, res) => {
  UploadFile.findAll().then(doc => {
    return res.status(200).json({ message: 'Success', obj: doc });
  });
});

// DOWNLOAD FILE ROUTE
router.post('/file', (req, res) => {
  const fileName = req.query.name;

  console.log(`filename: ${fileName}`);

  UploadFile.findOne({ where: { name: fileName } }).then(
    doc => {
      console.log(`doc: ${doc}`);
      if (doc !== null) {
        res.status(httpStatus.OK).sendFile(`${fileDirectory}/${fileName}`);
      } else {
        res.status(httpStatus.NOT_FOUND).json({
          message: 'Error',
          info: 'File was not found on our servers'
        });
      }
    },
    err => {
      console.log(err);
      res
        .status(httpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: 'error', info: 'an unexpected error occurred' });
    }
  );
});

// DELETE FILE ROUTE
router.delete('/deleteFile', (req, res) => {
  const fileName = req.query.name;

  fs.unlink(`${fileDirectory}/${fileName}`, err => {
    if (err) {
      console.log(err);
      res
        .status(httpStatus.INTERNAL_SERVER_ERROR)
        .json({
          info: 'error',
          message: 'An unexpected error occurred. Please try again later'
        })
        .send();
    } else {
      UploadFile.destroy({ where: { name: fileName } }).then(num => {
        console.log(num);
        if (num !== 0) {
          res
            .status(httpStatus.OK)
            .json({
              info: 'Success',
              message: 'File deleted successfully'
            })
            .send();
        } else {
          res
            .status(httpStatus.BAD_REQUEST)
            .json({
              info: 'Error',
              message: 'Unable to delete file. Please try again later'
            })
            .send();
        }
      });
    }
  });
});

/**
 * Middleware to check if the user is a valid user before processing the request.
 * It parses the token it received from the client to see if it is valid.
 * @param req the incoming request from the client
 * @param res the response to be sent
 * @param next a call to the next route
 *
 * @return 'Session expired if the token is invalid'
 * calls next() if the token is valid
 */

function verifyUser(req, res, next) {
  const authHeader = req.headers['authorization'];

  if (typeof authHeader === undefined) {
    return res.status(403).send();
  } else {
    const bearer = authHeader.split(' ')[1];
    jwt.verify(bearer, config.secretToken, (error, authData) => {
      if (error) {
        console.log(`error is: ${error}`);
        return res
          .status(500)
          .json({ message: 'Your session has expired. Please login' });
      }
      if (authData) {
        res.user_id = authData.data.id;
        next();
      }
    });
  }
}

module.exports = router;
