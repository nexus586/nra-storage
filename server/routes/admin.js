const express = require('express');
const router = express.Router();
const User = require('../models/users');
const bcrypt = require('bcryptjs');
const config = require('../config/config');
const httpStatus = require('http-status');
const _ = require('lodash');

router.post('/addUser', (req, res) => {
  const { username, password, name, role } = _.pick(req.body, [
    'username',
    'password',
    'name',
    'role'
  ]);

  const user = {
    username: username,
    name: name,
    role: role
  };

  const salt = bcrypt.genSaltSync();
  const hash = bcrypt.hashSync(password, salt);

  user.salt = salt;
  user.hash = hash;

  User.findOne({ where: { username: username } }).then(
    doc => {
      if (doc) {
        return res
          .status(httpStatus.CONFLICT)
          .json({ message: 'user already exists' });
      } else {
        User.create(user).then(
          doc => {
            res.status(httpStatus.OK).json({
              message: 'User created successfully',
              obj: doc
            });
          },
          error => {
            console.log(`unable to create user: ${error}`);
            res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
              message: 'Unable to create user. Please try again later',
              error: error
            });
          }
        );
      }
    },
    error => {
      console.log(`An unexpected error occurred: ${error}`);
      res
        .status(httpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: 'An unexpected error occurred', error });
    }
  );
});

router.get('/findAllUsers', (req, res) => {
  User.findAll().then(doc => {
    res.status(200).json({ msg: 'success', obj: doc });
  });
});

router.delete('/deleteUser', (req, res) => {
  const email = req.query.email;

  console.log(email);

  User.destroy({ where: { username: email } }).then(
    response => {
      console.log(response);
      res.status(httpStatus.OK).json({
        info: 'User deleted successfully'
      });
    },
    error => {
      console.log(error);
      res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
        message: 'An unexpected error occurred. Please try again later'
      });
    }
  );
});

module.exports = router;
