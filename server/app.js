require('dotenv').config();
const express = require('express');
const app = express();
const db = require('./models/db');
const userRoutes = require('./routes/user');
const adminRoutes = require('./routes/admin');
const jwt = require('jsonwebtoken');
const config = require('./config/config');
// const bodyParser = require('body-parser');
const port = process.env.PORT;
const cors = require('cors');

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(cors());

app.use('/api/v1/users', userRoutes);
app.use('/api/v1/admin', verifyAdmin, adminRoutes);

function verifyAdmin(req, res, next) {
  const authHeader = req.headers['authorization'];

  if (typeof authHeader === undefined) {
    return res.status(403).send();
  } else {
    const bearer = authHeader.split(' ')[1];
    jwt.verify(bearer, config.secretToken, (error, authData) => {
      if (error) {
        return res
          .status(500)
          .json({ message: 'Your session has expired. Please login' });
      }
      if (authData) {
        if (authData.data.role !== 'Admin') {
          return res.status(403).send();
        } else {
          next();
        }
      }
    });
  }
}

app.listen(port, () => {
  console.log(`listening on port:${port}`);
});
