const Sequelize = require('sequelize');
const Model = Sequelize.Model;
const sequelize = require('./db');
const bcrypt = require('bcryptjs');

class User extends Model {}

User.init(
  {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    hash: {
      type: Sequelize.STRING,
      allowNull: false
    },
    role: {
      type: Sequelize.STRING,
      allowNull: false
    },
    username: {
      type: Sequelize.STRING,
      allowNull: false
    },
    firstLoginAttempt: {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
    salt: {
      type: Sequelize.STRING,
      allowNull: false
    }
  },
  {
    sequelize,
    modelName: 'users'
  }
);

User.sync().then(() => {
  User.findOne({ where: { username: process.env.ADMIN_EMAIL } }).then(doc => {
    if (!doc) {
      const salt = bcrypt.genSaltSync();
	console.log(salt);
console.log(process.env.ADMIN_PASSWORD);
      const password = bcrypt.hashSync(process.env.ADMIN_PASSWORD, salt);

      User.create({
        username: process.env.ADMIN_EMAIL,
        name: process.env.ADMIN_NAME,
        hash: password,
        salt: salt,
        role: 'Admin'
      }).then(
        res => {
          console.log('admin user created successfully');
        },
        error => {
          console.log(`error creating admin: ${error}`);
        }
      );
    }
  });
});

module.exports = User;
