const Sequelize = require('sequelize');

const db_user_name = process.env.DB_USER_NAME;
const db_name = process.env.DB_NAME;
const db_password = process.env.DB_PASSWORD;
const db_host = process.env.DB_HOST;
const db_dialect = process.env.DB_DIALECT;

const sequelize = new Sequelize(db_name, db_user_name, db_password, {
  host: db_host,
  dialect: db_dialect
});

sequelize
  .authenticate()
  .then(() => {
    console.log('connection has been established successfully');
  })
  .catch(err => {
    console.log(`An unexpected error occurred: ${err}`);
  });

module.exports = sequelize;
