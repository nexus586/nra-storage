const Sequelize = require('sequelize');
const Model = Sequelize.Model;
const User = require('./users');
const sequelize = require('./db');

class UploadFile extends Model {}

UploadFile.init(
  {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    size: {
      type: Sequelize.BIGINT,
      allowNull: false
    },
    uploadedBy: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: User,
        key: 'id'
      }
    },
    mimeType: {
      type: Sequelize.STRING,
      allowNull: false
    },
    encoding: {
      type: Sequelize.STRING,
      allowNull: false
    }
  },
  {
    sequelize,
    modelName: 'uploaded_files'
  }
);

UploadFile.sync().then(
  res => {
    console.log('table created successfully');
  },
  err => {
    console.error(`an unexpected error occurred: ${err}`);
  }
);

module.exports = UploadFile;
